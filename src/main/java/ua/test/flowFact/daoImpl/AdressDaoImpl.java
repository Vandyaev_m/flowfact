package ua.test.flowFact.daoImpl;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ua.test.flowFact.dao.AdressDao;
import ua.test.flowFact.model.Adress;
import ua.test.flowFact.model.PhoneNumber;

@Transactional
@Component
public class AdressDaoImpl implements AdressDao {
	
	@Autowired
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void createAdress(Adress adress) {
		Session session = sessionFactory.getCurrentSession();
		Set<PhoneNumber> phoneNumbers = adress.getPhoneNumber();
		for (PhoneNumber number : phoneNumbers) {
			number.setAdress(adress);
		}
		session.persist(adress);
	}

	@Override
	public List<Adress> readAllAdresses() {
		List<Adress> adressList = sessionFactory.getCurrentSession().createQuery("from Adress").list();
		return adressList;
	}

	@Override
	public Adress readConcreteAdress(Integer id) {
		return (Adress) sessionFactory.getCurrentSession().get(Adress.class, id);
	}

	@Override
	public void updateAdress(Adress adress) {
		Session session = sessionFactory.getCurrentSession();
		Set<PhoneNumber> phoneNumbers = adress.getPhoneNumber();
		for (PhoneNumber number : phoneNumbers) {

			number.setAdress(adress);
		}
		session.update(adress);
	}

	@Override
	public void deleteAdress(Adress adress) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(adress);
	}

}
