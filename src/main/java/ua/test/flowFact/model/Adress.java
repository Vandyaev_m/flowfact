package ua.test.flowFact.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Adress")
public class Adress{

	@Id 
	@GeneratedValue
	@Column(name = "adress_id")
	private int adress_id;
	
	@Column(name = "streetName")
	private String streetName;
	
	@Column(name = "buildingNumber")
	private Integer buildingNumber;
	
	@Column(name = "apartamentNumber")
	private Integer apartamentNumber;

	
	@OneToMany(cascade = CascadeType.ALL , mappedBy="adress", fetch = FetchType.EAGER)
	private Set<PhoneNumber> phoneNumber;

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public Integer getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(Integer buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public Integer getApartamentNumber() {
		return apartamentNumber;
	}

	public void setApartamentNumber(Integer apartamentNumber) {
		this.apartamentNumber = apartamentNumber;
	}

	public Set<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Set<PhoneNumber> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getAdress_id() {
		return adress_id;
	}

	public void setAdress_id(int adress_id) {
		this.adress_id = adress_id;
	}

	@Override
	public String toString() {
		return "Adress [id=" + adress_id + ", streetName=" + streetName
				+ ", buildingNumber=" + buildingNumber + ", apartamentNumber="
				+ apartamentNumber + ", phoneNumber=" + phoneNumber + "]";
	}
	
}
