package ua.test.flowFact.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PhoneNumber")
public class PhoneNumber{

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name = "phone")
	private String phone;

	@ManyToOne
	@JoinColumn(name="adress_id")
	private Adress adress;
	
	public PhoneNumber() {

	}

	public PhoneNumber(String phoneNumber) {
		super();
		this.phone = phoneNumber;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null){
			return false;
		}
		if(this.getClass().equals(obj.getClass())){
			PhoneNumber phoneNumber = (PhoneNumber) obj;
			if(this.getPhone().equals(phoneNumber.getPhone())){
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.phone.hashCode();
	}

	@Override
	public String toString() {
		return this.phone;
	}

	
}
