package ua.test.flowFact.dao;

import java.util.List;

import ua.test.flowFact.model.Adress;

public interface AdressDao {

	public void createAdress(Adress adress);
	
	public List<Adress> readAllAdresses();
	
	public Adress readConcreteAdress(Integer id);
	
	public void updateAdress(Adress adress);
	
	public void deleteAdress(Adress adress);
	
}
