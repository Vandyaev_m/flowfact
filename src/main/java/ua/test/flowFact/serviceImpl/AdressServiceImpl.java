package ua.test.flowFact.serviceImpl;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ua.test.flowFact.dao.AdressDao;
import ua.test.flowFact.model.Adress;
import ua.test.flowFact.model.PhoneNumber;
import ua.test.flowFact.service.AdressService;

@Component
public class AdressServiceImpl implements AdressService {

	@Autowired
	private AdressDao adressDAO;
	
	public static final Logger LOGGER = LoggerFactory.getLogger(AdressServiceImpl.class);

	
	public void setAdressDAO(AdressDao adressDAO) {
		this.adressDAO = adressDAO;
	}

	@Override
	@Transactional
	public void createAdress(String streetName, Integer buildingNumber,
			Integer apartamentNumber, Set<PhoneNumber> phoneNumber) {
		checkPhone(phoneNumber);
		Adress adress = new Adress();
		adress.setStreetName(streetName);
		adress.setBuildingNumber(buildingNumber);
		adress.setApartamentNumber(apartamentNumber);
		adress.setPhoneNumber(phoneNumber);
		getAdressDAO().createAdress(adress);
	}

	@Override
	@Transactional
	public void updtateAdress(Adress adressToUpdate, String streetName,
			Integer buildingNumber, Integer apartamentNumber,
			Set<PhoneNumber> phoneNumber) {
		checkPhone(phoneNumber);
		Adress adress = getAdressDAO().readConcreteAdress(adressToUpdate.getAdress_id());
		adress.setStreetName(streetName);
		adress.setApartamentNumber(apartamentNumber);
		adress.setBuildingNumber(buildingNumber);
		if(adress.getPhoneNumber()!=null){
			adress.getPhoneNumber().addAll(phoneNumber);
		}else{
			adress.setPhoneNumber(phoneNumber);
		}
		getAdressDAO().updateAdress(adress);
		
	}

	@Override
	@Transactional
	public void deleteAdress(Integer adressId) {
		Adress adress = getAdressDAO().readConcreteAdress(adressId);
		
		if(adress!=null){
			getAdressDAO().deleteAdress(adress);
		}else{
			LOGGER.debug("Not found adress with id:"+adressId);
		}
	}

	@Override
	public List<Adress> showAll() {
		return getAdressDAO().readAllAdresses();
	}

	public AdressDao getAdressDAO() {
		return adressDAO;
	}

	private void checkPhone(Set<PhoneNumber> phoneNumbers){
		for(PhoneNumber number : phoneNumbers){
			if(!number.getPhone().matches("^\\+[1-9]{1}[0-9]{3,14}$")){
				throw new IllegalArgumentException("phone:"+number.getPhone()+" not valid");
			}
		}
	}
	
}
