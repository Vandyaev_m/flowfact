package ua.test.flowFact.controller;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ua.test.flowFact.model.Adress;
import ua.test.flowFact.model.PhoneNumber;
import ua.test.flowFact.service.AdressService;

import com.google.common.collect.Sets;

@Controller
public class AdressController {
	
	@Autowired
	private AdressService adressService;
	
	public static final Logger LOGGER = LoggerFactory.getLogger(AdressController.class);
	
	@RequestMapping("/index")
	public String helloWorld(Map<String, Object> map) {
		map.put("phoneNumber", new PhoneNumber());
		map.put("adress", new Adress());
		map.put("adressList", getAdressService().showAll());
		return "adressViewer";
	}
	
	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addContact(@ModelAttribute("adress") Adress adress,BindingResult result,
			@ModelAttribute("phoneNumber") PhoneNumber phoneNumber) {
		LOGGER.debug("Get some adress:"+ adress.toString());
		String phones[] = phoneNumber.getPhone().split(",");
		Set<PhoneNumber> numbers = Sets.newHashSet();
		for(String phone : phones){
			numbers.add(new PhoneNumber(phone));
		}
		
		if (adress.getStreetName() == null
				|| adress.getBuildingNumber() == null
				|| adress.getApartamentNumber() == null) {
			String message = "<br>"
					+ "<h3>null elements</h3><br><br>";
			return new ModelAndView("redirect:/index", "errMessage", message);
		}
		
		getAdressService().createAdress(adress.getStreetName(), adress.getBuildingNumber(), adress.getApartamentNumber(), numbers);

		return new ModelAndView("redirect:/index", "ErrMessage", "");
	}
	
	@RequestMapping("/delete/{adress_id}")
	public String deleteContact(@PathVariable("adress_id") Integer adressId) {

		LOGGER.debug("id of adress"+ adressId);

		getAdressService().deleteAdress(adressId);;

		return "redirect:/index";
	}
	

	public AdressService getAdressService() {
		return adressService;
	}
	
	
}
