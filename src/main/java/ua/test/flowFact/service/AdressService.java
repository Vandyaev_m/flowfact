package ua.test.flowFact.service;

import java.util.List;
import java.util.Set;

import ua.test.flowFact.model.Adress;
import ua.test.flowFact.model.PhoneNumber;

public interface AdressService {
	
	public void createAdress(String streetName, Integer buildingNumber, Integer apartamentNumber, Set<PhoneNumber> phoneNumber);
	
	public void updtateAdress(Adress adressToUpdate, String streetName, Integer buildingNumber, Integer apartamentNumber, Set<PhoneNumber> phoneNumber); 

	public void deleteAdress(Integer adressId);
	
	public List<Adress> showAll();

}
