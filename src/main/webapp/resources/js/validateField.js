$(document).ready(function () {
	<!--Name can't be blank-->
	$('#streetName').on('input', function() {
	    var input=$(this);
	    var is_name=input.val();
	    if(is_name){input.removeClass("invalid").addClass("valid");}
	    else{input.removeClass("valid").addClass("invalid");}
	});
	$('#buildingNumber').on('input', function() {
	    var input=$(this);
	    var re = /^[0-9]{1,4}[A-z]{0,1}$/;
	    var is_number=re.test(input.val());
	    if(is_number&&input.val()){input.removeClass("invalid").addClass("valid");}
	    else{input.removeClass("valid").addClass("invalid");}
	});
	
	$('#apartamentNumber').on('input', function() {
	    var input=$(this);
	    var re = /^[0-9]{1,4}$/;
	    var is_number=re.test(input.val());
	    if(is_number&&input.val()){input.removeClass("invalid").addClass("valid");}
	    else{input.removeClass("valid").addClass("invalid");}
	});
	
	$('#phone').on('input', function() {
	    var input=$(this);
	    var re = /^\+[1-9]{1}[0-9]{3,14}(\,\+[1-9]{1}[0-9]{3,14}){0,}$/;
	    var is_number=re.test(input.val());
	    if(is_number&&input.val()){input.removeClass("invalid").addClass("valid");}
	    else{input.removeClass("valid").addClass("invalid");}
	});
	
	$('#form').submit(function () {
		var form_data=$("#form").serializeArray();
	    var error_free=true;
	    for (var input in form_data){
	        var element=$("#"+form_data[input]['name']);
	        var valid=element.hasClass("valid");
	        var error_element=element.parent().next();
	        if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
	        else{error_element.removeClass("error_show").addClass("error");}
	    }
	    if (!error_free){
	    	return false;
	    }
	   
		});

});
