<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><spring:message code="label.title" /></title>
	<link href="<c:url value="/resources/css/adressViewer.css" />" rel="stylesheet">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="<c:url value="/resources/js/validateField.js"/>"></script>
</head>
<body>
<h2><spring:message code="label.title" /></h2>

<form:form method="post" action="add" commandName="adress" id="form">

	<table>
		<tr>
			<td><form:label path="streetName">
				<spring:message code="label.streetAlias" />
			</form:label></td>
			<td><form:input path="streetName" id="streetName"/></td>
			<td class="error"><spring:message code="label.fieldNotEmpty" /></td>
		</tr>
		<tr>
			<td><form:label path="buildingNumber">
				<spring:message code="label.buildingNumber" />
			</form:label></td>
			<td><form:input path="buildingNumber" id = "buildingNumber" /></td>
			<td class="error"><spring:message code="label.fieldNumber" /></td>
		</tr>
		<tr>
			<td><form:label path="apartamentNumber">
				<spring:message code="label.apartamentNumber" />
			</form:label></td>
			<td><form:input  path="apartamentNumber" id = "apartamentNumber"/></td>
			<td class="error"><spring:message code="label.fieldNumber" /></td>
		</tr>
		<form:form method="post" action="add" commandName="phoneNumber">
				<tr>
					<td><form:label path="phone">
						<spring:message code="label.phoneNumbers" />
					</form:label></td>
					<td><form:textarea path="phone" class="phone"/></td>
					<td class="error"><spring:message code="label.phoneerr" /></td>
				</tr>
		</form:form>
		<tr>
			<td colspan="2"><c:out value="${errMessage}" escapeXml="false" /></td>
		</tr>		

		<tr>
			<td colspan="2"><input type="submit"
				value="<spring:message code="label.addadress"/>" /></td>
		</tr>
	</table>
</form:form>


<h3><spring:message code="label.Adress" /></h3>
<c:if test="${!empty adressList}">
	<table cellspacing="15" class="data">
		<tr>
			<th><spring:message code="label.streetAlias" /></th>
			<th><spring:message code="label.buildingNumber" /></th>
			<th><spring:message code="label.apartamentNumber" /></th>
			<th><spring:message code="label.phoneNumbers" /></th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${adressList}" var="adress">
			<tr>
				<td>${adress.streetName}</td>
				<td align="center">${adress.buildingNumber}</td>
				<td align="center">${adress.apartamentNumber}</td>
				<td>
					<c:forEach items="${adress.phoneNumber}" var="phNumb">
						${phNumb.phone}
					</c:forEach>
				</td>
				<td><a href="delete/${adress.adress_id}"><spring:message code="label.delete" /></a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>
</body>
</html>