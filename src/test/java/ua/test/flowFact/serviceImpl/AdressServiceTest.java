package ua.test.flowFact.serviceImpl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ua.test.flowFact.model.Adress;
import ua.test.flowFact.model.PhoneNumber;
import ua.test.flowFact.service.AdressService;

import com.google.common.collect.Sets;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:META-INF/data.xml" })
public class AdressServiceTest {

	@Autowired
	private AdressService adressService;
	

	@Test(expected = IllegalArgumentException.class)
	public void addIllegalPhone(){
		getAdressService().createAdress("Dimitrova", 5, 28, Sets.newHashSet(new PhoneNumber("+lberlvhbbrev"),new PhoneNumber("+380931454755")));
	}
	
	@Test
	public void createTest(){
		getAdressService().createAdress("Dimitrova", 5, 28, Sets.newHashSet(new PhoneNumber("+380931454759"),new PhoneNumber("+380931454755")));
		getAdressService().createAdress("Boiko", 4, 12, Sets.newHashSet(new PhoneNumber("+380931234123")));
		List<Adress> adresses = getAdressService().showAll();
		Assert.assertNotNull(adresses);
		Assert.assertEquals(2, adresses.size());
	}

	@Test
	public void deleteTest() throws InterruptedException{
		List<Adress> adresses = getAdressService().showAll();
		getAdressService().deleteAdress(adresses.get(0).getAdress_id());
		adresses = getAdressService().showAll();
		Assert.assertNotNull(adresses);
		Assert.assertEquals(1, adresses.size());
		getAdressService().updtateAdress(adresses.get(0), "Dimitrova", 5, 28, Sets.newHashSet(new PhoneNumber("+380931454759"),new PhoneNumber("+380931454755")));
		Assert.assertNotNull(adresses);
		Assert.assertEquals(1, adresses.size());
	}
	

	public AdressService getAdressService() {
		return adressService;
	}
}
